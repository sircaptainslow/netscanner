﻿using System;
using System.Net;
using System.IO;

namespace VersionManager
{
	public class Version
	{
		private const String availableVersionUri = "https://bytebucket.org/moygrafik/moygrafikterminal/wiki/Version.txt";

		private Int32 VersionNumber;

		private Version( Int32 major, Int32 minor, Int32 build )
		{
			this.VersionNumber = major * 1000000 + minor * 1000 + build;
		}

		private Version( Int32 versionNumber )
		{
			this.VersionNumber = versionNumber;
		}

		public override String ToString()
		{
			var major = this.VersionNumber / 1000000;
			var minor = this.VersionNumber / 1000 % 1000;
			var build = this.VersionNumber % 1000;

			return String.Format( "{0}.{1}.{2}", major, minor, build );
		}

		public static Version Current()
		{
			return new Version( 1, 1, 12 );
		}

		public static Version Available()
		{
			var tempPath = Path.GetTempFileName();

			using ( var client = new WebClient() )
			{
				client.DownloadFile( availableVersionUri, tempPath );
			}

			using ( var r = File.OpenText( tempPath ) )
			{
				return new Version( Int32.Parse( r.ReadLine() ) );
			}
		}

		public static bool operator < ( Version a, Version b )
		{
			return ( a.VersionNumber < b.VersionNumber );
		}

		public static bool operator > ( Version a, Version b )
		{
			return ( a.VersionNumber > b.VersionNumber );
		}

		public static bool operator == ( Version a, Version b )
		{
			return ( a.VersionNumber == b.VersionNumber );
		}

		public static bool operator != ( Version a, Version b )
		{
			return !( a == b );
		}
	}
}

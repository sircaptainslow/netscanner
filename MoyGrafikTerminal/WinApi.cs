﻿using System;
using System.Runtime.InteropServices;

namespace WinApi
{
	public class IpHelper
	{
		#region КОНСТАНТЫ

		public const Int16 AF_INET = 2;

		#endregion

		#region ПЕРЕЧИСЛЕНИЯ

		public enum NL_NEIGHBOR_STATE
		{
			NlnsUnreachable,
			NlnsIncomplete,
			NlnsProbe,
			NlnsDelay,
			NlnsStale,
			NlnsReachable,
			NlnsPermanent,
			NlnsMaximum
		}

		#endregion

		#region СТРУКТУРЫ
		
		//======================================================================

		[StructLayout( LayoutKind.Sequential )]
		public class SOCKADDR_IN
		{
			public Int16 sin_family;
			public UInt16 sin_port;
			public UInt32 sin_addr;

			[MarshalAs( UnmanagedType.ByValArray, SizeConst = 8 )]
			public Byte[] sin_zero;

			public SOCKADDR_IN()
			{
				this.sin_zero = new Byte[8];
			}
		}

		//======================================================================

		[StructLayout( LayoutKind.Sequential )]
		public class SOCKADDR_IN6
		{
			Int16	sin6_family;
			UInt16	sin6_port;
			UInt32	sin6_flowinfo;

			[MarshalAs( UnmanagedType.ByValArray, SizeConst = 16 )]
			Byte[]	sin6_addr;

			UInt32	sin6_scope_id;

			public SOCKADDR_IN6()
			{
				this.sin6_addr = new Byte[16];
			}
		}

		//======================================================================

		[StructLayout( LayoutKind.Explicit, Size = 0x1c )]
		public class SOCKADDR_INET
		{
			[FieldOffset( 0 )]
			public SOCKADDR_IN Ipv4;

			//[FieldOffset( 0 )]
			//public SOCKADDR_IN6 Ipv6;

			public SOCKADDR_INET()
			{
				//this.Ipv6 = new SOCKADDR_IN6();
				this.Ipv4 = new SOCKADDR_IN();
			}
		}

		//======================================================================

		[StructLayout( LayoutKind.Sequential )]
		public class MIB_IPNET_ROW2
		{
			public SOCKADDR_INET Address;
			public UInt32 InterfaceIndex;
			public UInt64 InterfaceLuid;

			[MarshalAs( UnmanagedType.ByValArray, SizeConst = 32 )]
			public Byte[] PhysicalAddress;

			public UInt32 PhysicalAddressLength;
			public NL_NEIGHBOR_STATE State;
			public Byte Flags;
			public UInt32 LastReachable;

			public MIB_IPNET_ROW2()
			{
				this.Address = new SOCKADDR_INET();
				this.PhysicalAddress = new Byte[32];
			}
		}

		//======================================================================

		[StructLayout(LayoutKind.Sequential)]    
		public class MIB_IPNET_TABLE2
		{    
		    UInt32 NumEntries;
			MIB_IPNET_ROW2 Table;
		}

		//======================================================================

		#endregion

		#region ФУНКЦИИ

		[DllImport( "IpHlpApi.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall )]
		public static extern
		Int32 SendARP(
			Int32		destIp,
			Int32		srcIp,
			Byte[]		MacAddress,
			ref UInt32	MacAddressLength
		);
		
		[DllImport( "IpHlpApi.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall )]
		public static extern
		int ResolveIpNetEntry2(
			MIB_IPNET_ROW2	Row,
			SOCKADDR_INET	SourceAddress
		);

		[DllImport( "IpHlpApi.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall )]
		public static extern
		int GetIpNetTable2(
			Int16 Family,
			out IntPtr Table
		);

		[DllImport( "IpHlpApi.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall )]
		public static extern
		void FreeMibTable(
			IntPtr Table
		);

		#endregion
	}
}

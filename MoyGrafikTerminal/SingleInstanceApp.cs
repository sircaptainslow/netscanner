﻿using System;
using Microsoft.VisualBasic.ApplicationServices;

namespace MoyGrafikTerminal
{
	public class SingleInstanceApp : WindowsFormsApplicationBase
	{
		private SingleInstanceApp()
		{ 
			IsSingleInstance = true; 
		}
    
		public static void Run()
		{
			SingleInstanceApp app = new SingleInstanceApp();

			app.StartupNextInstance += delegate
			{
				app.MainForm.Show();
			};

			app.Run( Environment.GetCommandLineArgs() );
		}

		protected override void OnCreateMainForm()
		{
			this.MainForm = new MainForm();
		}
	}
}

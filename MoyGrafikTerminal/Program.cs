﻿
using System;
using System.Windows.Forms;

namespace MoyGrafikTerminal
{
	public class Program
	{
		public const String SetupUrl = "https://bytebucket.org/moygrafik/moygrafikterminal/wiki/setupmgt.exe";

		static void Main( String[] args )
		{
			Application.EnableVisualStyles();
			SingleInstanceApp.Run();
		}
	}
}

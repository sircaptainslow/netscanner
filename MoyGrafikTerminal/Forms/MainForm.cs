﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Http;
using System.IO;
using System.Diagnostics;

namespace MoyGrafikTerminal
{
	using Version = VersionManager.Version;

	public partial class MainForm : Form
	{
		#region ПОЛЯ

		// Событие завершения
		private ManualResetEvent Shutdown = null;

		// Событие перезапуска сканирования
		private ManualResetEvent Restart = null;

		// Определённые устройства
		private Dictionary<String, DeviceInfo> DevicesFound = null;

		// Настройки
		//private Settings Settings = null;

		private UnicastIPAddressInformation ChosenIpInformation;
		private NetworkInterface ChosenAdapter;

		// Признак окончательного закрытия программы
		private Boolean ForceQuit = false;

		private Int32 SecondsFromLastSend = 0;

		private System.Windows.Forms.Timer SendTimer = null;

		#endregion

		#region КОНСТРУКТОРЫ

		//======================================================================
		// Конструктор по-умолчанию
		//======================================================================

		public MainForm()
		{
			// Инициализация окна
			InitializeComponent();
			InitializeMenuBehaviour();
			InitializeBehaviour();

			// Загрузка настроек
			//this.Settings = Settings.Load();

			foreach ( var adapter in NetworkInterface.GetAllNetworkInterfaces() )
			{
				var props = adapter.GetIPProperties();
				var addresses = props.UnicastAddresses;

				foreach ( var address in addresses )
				{
					if ( address.Address.Equals( Settings.ChosenIpAddress ) )
					{
						// Нашли адаптер и адрес
						this.ChosenAdapter = adapter;
						this.ChosenIpInformation = address;
						break;
					}
				}

				if ( this.ChosenAdapter != null )
					break;
			}

			// Инициализация списка найденных устройств
			this.DevicesFound = new Dictionary<String, DeviceInfo>();

			// Инициализация события завершения
			this.Shutdown = new ManualResetEvent( false );

			this.Restart = new ManualResetEvent( false );

			// Увеличиваем пул нитей
			ThreadPool.SetMaxThreads( 200, 10 );
			ThreadPool.SetMinThreads( 200, 10 );

			// Запуск нити сканирования сети
			( new Thread( ScanThread ) ).Start();

			//( new Thread( SendThread ) ).Start();

			this.SendTimer = new System.Windows.Forms.Timer();

			this.SendTimer.Interval = 1000;
			this.SendTimer.Tick += SendTick;
			this.SendTimer.Start();
			
			( new Thread( UpdateThread ) ).Start();
		}

		//======================================================================

		#endregion

		#region НИТИ

		//======================================================================
		// Нить сканирования сети
		//======================================================================

		private void ScanThread()
		{
			var ipAddressBytes = this.ChosenIpInformation.Address.GetAddressBytes();
			var maskBytes = this.ChosenIpInformation.IPv4Mask.GetAddressBytes();

			for ( int i = 0; i < 4; i++ )
			{
				ipAddressBytes[i] &= maskBytes[i];
			}

			var firstIpAddress = new IPAddress( ipAddressBytes );
			
			for ( int i = 0; i < 4; i++ )
			{
				ipAddressBytes[i] |= ( byte ) ( ~maskBytes[i] );
			}

			var lastIpAddress = Utils.GetNextIpAddress( new IPAddress( ipAddressBytes ) );

			var ipAddress = firstIpAddress;

			while ( !this.Shutdown.WaitOne( 50 ) )
			{
				if ( this.Restart.WaitOne( 0 ) )
				{
					this.Invoke( ( MethodInvoker ) delegate
					{
						foreach ( var d in this.DevicesFound )
						{
							this.flowLayoutPanel1.Controls.Remove( d.Value );
						}
					});

					this.DevicesFound.Clear();
					return;
				}

				var t = new Task( () => this.CheckReachability( ipAddress ) );
				t.Start();

				ipAddress = Utils.GetNextIpAddress( ipAddress );

				if ( ipAddress.GetAddressBytes()[3] == 0xFF )
					ipAddress = Utils.GetNextIpAddress( ipAddress );

				if ( ipAddress.Equals( lastIpAddress ) )
				{
					ipAddress = firstIpAddress;
				}
			}
		}

		//======================================================================
		// Нить отправки данных на сервер
		//======================================================================

		private async void SendTick( Object sender, EventArgs args )
		{
			if ( this.SecondsFromLastSend == Settings.SendPeriod )
			{
				this.SecondsFromLastSend = 0;

				var macBytes = this.ChosenAdapter.GetPhysicalAddress().GetAddressBytes();
				var mac = Utils.MacAddressToString( macBytes );

				var devices = "{";

				if ( this.DevicesFound.Count > 0 )
				{
					foreach ( var device in this.DevicesFound )
					{
						devices += "\"" + Utils.MacAddressToString( device.Value.MacAddress ) + "\"";
						devices += ":" + "\"" + device.Value.IpAddress.ToString() + "\"";
						devices += ",";
					}

					devices = devices.Substring( 0, devices.Length - 1 );
				}

				devices += "}";

				var values = new Dictionary<String, String>
				{
					{ "device", "desktop" },
					{ "mac", mac },
					{ "ip", this.ChosenIpInformation.Address.ToString() },
					{ "devices", devices }
				};

				try {
					using ( var client = new HttpClient() )
					{
						var content = new FormUrlEncodedContent( values );
						var response = await client.PostAsync( Settings.ServerUri, content );

						this.toolStripStatusLabel2.Text = response.ReasonPhrase + " (" + ( ( int ) response.StatusCode ).ToString() + ")";
					}
				}

				catch ( Exception )
				{
					this.toolStripStatusLabel2.Text = "Ошибка";
				}

			} else {

				this.SecondsFromLastSend++;
			}

			var secondsLeft = Settings.SendPeriod - this.SecondsFromLastSend;

			if ( secondsLeft > 0 )
			{
				var timeLeft = String.Empty;

				if ( secondsLeft < 60 )
				{
					timeLeft = secondsLeft.ToString() + " сек.";
				} else if ( secondsLeft < 3600 ) {
					timeLeft = ( secondsLeft / 60 ).ToString() + " мин.";
				} else if ( secondsLeft < 86400 ) {
					timeLeft = ( secondsLeft / 3600 ).ToString() + " ч.";
				} else {
					timeLeft = ( secondsLeft / 86400 ).ToString() + " д.";
				}

				this.toolStripStatusLabel1.Text = "Отправка на сервер через: " + timeLeft;
			} else {
				this.toolStripStatusLabel1.Text = "Отправка...";
			}
		}

		//======================================================================
		// Нить проверки обновлений
		//======================================================================

		private void UpdateThread()
		{
			var LastVersion = Version.Current();

			do
			{
				var AvailableVersion = Version.Available();

				if ( AvailableVersion > LastVersion )
				{
					this.Invoke( ( MethodInvoker ) delegate
					{
						var message = String.Format( "Доступна версия {0}. Обновить?", AvailableVersion.ToString() );
						var result = MessageBox.Show( message, "MoyGrafik Terminal", MessageBoxButtons.YesNo );

						switch ( result )
						{
							case DialogResult.No:
								LastVersion = AvailableVersion;
								break;

							case DialogResult.Yes:
								var tempPath = Path.GetTempFileName() + ".exe";

								var task = new Task( () =>
								{
									using ( var client = new WebClient() )
									{
										client.DownloadFile( Program.SetupUrl, tempPath );
									}

									Process.Start( tempPath );

									this.ForceQuit = true;
									this.Invoke( ( MethodInvoker ) delegate
									{
										this.Close();
									});
								});

								task.Start();
								this.SendTimer.Stop();
								this.toolStripStatusLabel1.Text = "Загрузка обновления...";
								break;

							default:
								throw new Exception();
						}
					});
				}
			} while ( !this.Shutdown.WaitOne( 600 * 1000 ) );
		}

		//======================================================================

		#endregion

		#region МЕТОДЫ
		
		private void InitializeMenuBehaviour()
		{
			this.ExitToolStripMenuItem.Click += ( s, e ) =>
			{
				this.ForceQuit = true;
				this.Close();
			};

			this.SettingsToolStripMenuItem.Click += ( s, e ) =>
			{
				var settingsForm = new SettingsForm();
				settingsForm.ShowDialog();

				this.SendTimer.Stop();
				this.toolStripStatusLabel1.Text = "Применение настроек...";

				var t = new Thread( ( ThreadStart ) delegate {

					// Завершаем нить сканирования и ждём, пока завершатся все запросы
					this.Restart.Set();
					Thread.Sleep( 5000 );

					this.ChosenAdapter = null;
					this.ChosenIpInformation = null;

					foreach ( var adapter in NetworkInterface.GetAllNetworkInterfaces() )
					{
						var props = adapter.GetIPProperties();
						var addresses = props.UnicastAddresses;

						foreach ( var address in addresses )
						{
							if ( address.Address.Equals( Settings.ChosenIpAddress ) )
							{
								// Нашли адаптер и адрес
								this.ChosenAdapter = adapter;
								this.ChosenIpInformation = address;
								break;
							}
						}

						if ( this.ChosenAdapter != null )
							break;
					}

					this.Restart.Reset();
					( new Thread( ScanThread ) ).Start();
					this.SecondsFromLastSend = 0;

					this.Invoke( ( MethodInvoker ) delegate {
						this.SendTimer.Start();
					});
				});

				t.Start();
			};

			this.AboutToolStripMenuItem.Click += ( s, e ) =>
			{
				var aboutForm = new AboutForm();
				aboutForm.ShowDialog();

				if ( aboutForm.Updated )
				{
					this.ForceQuit = true;
					this.Close();
				}
			};
		}

		private void InitializeBehaviour()
		{
			this.flowLayoutPanel1.SizeChanged += ( s, e ) =>
			{
				foreach ( var control in this.flowLayoutPanel1.Controls )
				{
					var label = ( DeviceInfo ) control;
					var paddingLeft = this.flowLayoutPanel1.Padding.Left;
					var paddingRight = this.flowLayoutPanel1.Padding.Right;
					label.Width = this.flowLayoutPanel1.Width - paddingLeft - paddingRight;
				}
			};

			this.flowLayoutPanel1.ControlAdded += ( s, e ) =>
			{
				foreach ( var control in this.flowLayoutPanel1.Controls )
				{
					var label = ( DeviceInfo ) control;
					var paddingLeft = this.flowLayoutPanel1.Padding.Left;
					var paddingRight = this.flowLayoutPanel1.Padding.Right;
					label.Width = this.flowLayoutPanel1.Width - paddingLeft - paddingRight;
				}
			};

			this.notifyIcon1.MouseClick += ( s, e ) =>
			{
				if ( e.Button == MouseButtons.Left )
				{
					this.Show();
					if ( this.WindowState == FormWindowState.Minimized )
					{
						this.WindowState = FormWindowState.Normal;
					}
				}
			};

			this.ExitTrayToolStripMenuItem.Click += ( s, e ) =>
			{
				this.ForceQuit = true;
				this.Close();
			};

			this.FormClosing += ( s, e ) =>
			{
				if ( this.ForceQuit )
				{
					this.Shutdown.Set();
				} else {
					e.Cancel = true;
					this.Hide();
				}
			};

			this.Resize += ( s, e ) =>
			{
				if ( this.WindowState == FormWindowState.Minimized )
				{
					this.Hide();
				}
			};
		}

		private void CheckReachability( IPAddress ipAddress )
		{
			if ( ipAddress.Equals( this.ChosenIpInformation.Address ) )
			{
				var macBytes = this.ChosenAdapter.GetPhysicalAddress().GetAddressBytes();

				this.AddDevice( macBytes, ipAddress );
				return;
			}

			using ( var ping = new Ping() )
			{
				var reply = ping.Send( ipAddress );
			}

			var arpTable = Utils.GetArpTable( this.ChosenIpInformation.Address );

			if ( arpTable.ContainsKey( ipAddress ) )
			{
				this.AddDevice( arpTable[ipAddress], ipAddress );
			} else {
				this.RemoveDevice( ipAddress );
			}
		}

		//======================================================================
		// Добавление устройства в список
		//======================================================================

		private void AddDevice( byte[] macAddress, IPAddress ipAddress )
		{
			if ( this.Restart.WaitOne( 0 ) )
				return;

			try
			{
				var ipAddressString = ipAddress.ToString();

				this.Invoke( ( MethodInvoker ) delegate
				{
					var macString = macAddress[0].ToString( "X2" );

					for ( int i = 1; i < 6; i++ )
					{
						macString += ":" + macAddress[i].ToString( "X2" );
					}

					var text = String.Format( "IP-адрес: {0}\nMAC-адрес: {1}", ipAddress, macString );

					if ( this.DevicesFound.ContainsKey( ipAddressString ) )
					{
						this.DevicesFound[ipAddressString].MacAddress = macAddress;
						this.DevicesFound[ipAddressString].IpAddress = ipAddress;
						this.DevicesFound[ipAddressString].Text = text;
					} else {
						var deviceInfo = new DeviceInfo( macAddress, ipAddress );
						this.DevicesFound[ipAddressString] = deviceInfo;
						this.flowLayoutPanel1.Controls.Add( deviceInfo );
					}
				});
			}

			catch { }
		}

		//======================================================================
		// Удаление устройства из списка
		//======================================================================

		private void RemoveDevice( IPAddress ipAddress )
		{
			try
			{
				var ipAddressString = ipAddress.ToString();

				this.Invoke( ( MethodInvoker ) delegate
				{
					if ( this.DevicesFound.ContainsKey( ipAddressString ) )
					{
						var label = this.DevicesFound[ipAddressString];
						this.flowLayoutPanel1.Controls.Remove( label );
						this.DevicesFound.Remove( ipAddressString );
					}
				});
			}

			catch { }
		}

		//======================================================================

		#endregion
	}
}

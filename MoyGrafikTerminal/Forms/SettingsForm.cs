﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;

namespace MoyGrafikTerminal
{
	public partial class SettingsForm : Form
	{
		class ComboBoxItem
		{
			public String Text;
			public Object Value;

			public ComboBoxItem( String text, Object value )
			{
				this.Text = text;
				this.Value = value;
			}

			public override String ToString()
			{
				return this.Text;
			}
		}

		public SettingsForm()
		{
			InitializeComponent();

			this.ControlBox = false;

			this.button1.Click += ( s, e ) =>
			{
				Settings.ServerUri = new Uri( this.textBox1.Text );
				Settings.SendPeriod = ( int ) this.numericUpDown1.Value;
				var item = ( ComboBoxItem ) this.comboBox1.SelectedItem;
				Settings.ChosenIpAddress = ( IPAddress ) item.Value;
				this.Close();
			};

			foreach ( var adapter in NetworkInterface.GetAllNetworkInterfaces() )
			{
				if ( adapter.NetworkInterfaceType != NetworkInterfaceType.Loopback &&
					 adapter.NetworkInterfaceType != NetworkInterfaceType.Tunnel )
				{
					foreach ( var ip in adapter.GetIPProperties().UnicastAddresses )
					{
						if ( ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
						{
							var item = new ComboBoxItem( adapter.Name + " (" + ip.Address.ToString() + ")", ip.Address );
							this.comboBox1.Items.Add( item );

							if ( ip.Address.Equals( Settings.ChosenIpAddress ) )
							{
								this.comboBox1.SelectedItem = item;
							}
						}
					}
				}
			}

			this.textBox1.Text = Settings.ServerUri.ToString();
			this.numericUpDown1.Value = Settings.SendPeriod;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace MoyGrafikTerminal
{
	using Version = VersionManager.Version;

	public partial class AboutForm : Form
	{
		public bool Updated = false;

		public AboutForm()
		{
			InitializeComponent();
			
			this.ProductLabel.Text += " " + Version.Current().ToString();

			if ( Version.Available() > Version.Current() )
			{
				this.button1.Enabled = true;
			} else {
				this.button1.Enabled = false;
			}

			this.button1.Click += ( s, e ) =>
			{
				var tempPath = Path.GetTempFileName() + ".exe";

				using ( var client = new WebClient() )
				{
					client.DownloadFile( Program.SetupUrl, tempPath );
				}

				this.Updated = true;
				Process.Start( tempPath );
				this.Close();
			};
		}
	}
}

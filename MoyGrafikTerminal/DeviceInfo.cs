﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Net;

namespace MoyGrafikTerminal
{
	public class DeviceInfo : Panel
	{
		private Label Label;

		public byte[] MacAddress;
		public IPAddress IpAddress;

		public override String Text
		{
			get { return this.Label.Text; }
			set { this.Label.Text = value; }
		}

		public DeviceInfo( byte[] macAddress, IPAddress ipAddress)
		{
			this.MacAddress = macAddress;
			this.IpAddress = ipAddress;

			this.Label = new Label();
			this.Label.Dock = DockStyle.Fill;
			this.Label.TextAlign = ContentAlignment.MiddleCenter;
			this.Label.Text = String.Format( "IP-адрес: {0}\nMAC-адрес: {1}", ipAddress, Utils.MacAddressToString( macAddress ) );
			this.Controls.Add( this.Label );

			this.BackColor = Color.FromArgb( 0x90, 0xca, 0xf9 );
			this.Height = 50;
			this.Margin = new Padding( 0, 0, 0, 15 );
			this.Visible = true;
		}
	}
}

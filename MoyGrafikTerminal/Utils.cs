﻿using System;
using System.Runtime.InteropServices;
using System.Net;
using System.Collections.Generic;
using WinApi;

namespace MoyGrafikTerminal
{
	public class Utils
	{
		//======================================================================
		// Получение следующего по-порядку IP-адреса
		//======================================================================

		public static
		IPAddress GetNextIpAddress(
			IPAddress ipAddress
			)
		{
			var bytes = ipAddress.GetAddressBytes();

			for ( int i = 3; i >= 0; i-- )
			{
				if ( ++bytes[i] != 0 )
					break;
			}

			return ( new IPAddress( bytes ) );
		}

		//======================================================================
		// Преобразование MAC-адреса в строку
		//======================================================================

		public static
		String MacAddressToString(
			byte[] macAddress
			)
		{
			if ( macAddress == null )
				throw new NullReferenceException();

			if ( macAddress.Length < 6 )
				throw new ArgumentException();

			var mac = macAddress[0].ToString( "X2" );

			for ( int i = 1; i < 6; i++ )
			{
				mac += ":" + macAddress[i].ToString( "X2" );
			}

			return mac;
		}

		//======================================================================
		// Получение списка IP-адресов из ARP-таблицы указанного адаптера
		//======================================================================
		
		public static
		Dictionary<IPAddress, byte[]> GetArpTable(
			IPAddress adapterIpAddress
			)
		{
			var arpTable = new Dictionary<IPAddress, byte[]>();

			IntPtr Table;
			IntPtr Current;

			IpHelper.GetIpNetTable2( IpHelper.AF_INET, out Table );

			Current = Table;

			UInt32 Count = ( UInt32 ) Marshal.PtrToStructure( Current, typeof( UInt32 ) );
			Current += 8;

			for ( int i = 0; i < Count; i++ )
			{
				var ip = ( IpHelper.MIB_IPNET_ROW2 ) Marshal.PtrToStructure( Current, typeof( IpHelper.MIB_IPNET_ROW2 ) );
				Current += Marshal.SizeOf( typeof( IpHelper.MIB_IPNET_ROW2 ) );

				switch ( ip.State )
				{
					case IpHelper.NL_NEIGHBOR_STATE.NlnsReachable:
					case IpHelper.NL_NEIGHBOR_STATE.NlnsStale:
					case IpHelper.NL_NEIGHBOR_STATE.NlnsPermanent:
					case IpHelper.NL_NEIGHBOR_STATE.NlnsDelay:
					case IpHelper.NL_NEIGHBOR_STATE.NlnsProbe:
						var ipAddress = new IPAddress( ip.Address.Ipv4.sin_addr );
						arpTable[ipAddress] = ip.PhysicalAddress;
						break;
				}
			}

			IpHelper.FreeMibTable( Table );

			return arpTable;
		}

		//======================================================================
	}
}

﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace MoyGrafikTerminal
{
	public class Settings
	{
		#region СВОЙСТВА
		
		//======================================================================
		// Выбранный IP-адрес
		//======================================================================

		public static
		IPAddress ChosenIpAddress
		{
			get
			{
				var ipString = Properties.Settings.Default.ChosenIpAddress;

				try
				{
					// TODO: проверка на присутствие в системе
					return IPAddress.Parse( ipString );
				}

				catch
				{
					var ip = SelectInterface();
					Properties.Settings.Default.ChosenIpAddress = ip.ToString();
					return ip;
				}
			}

			set
			{
				Properties.Settings.Default.ChosenIpAddress = value.ToString();
				Properties.Settings.Default.Save();
			}
		}

		//======================================================================
		// URI для отправки данных на сервер
		//======================================================================

		public static
		Uri ServerUri
		{
			get
			{
				return new Uri( Properties.Settings.Default.ServerUri );
			}

			set
			{
				Properties.Settings.Default.ServerUri = value.ToString();
				Properties.Settings.Default.Save();
			}
		}

		//======================================================================
		// Период отправки данных на сервер
		//======================================================================

		public static
		Int32 SendPeriod
		{
			get
			{
				return Properties.Settings.Default.SendPeriod;
			}

			set
			{
				Properties.Settings.Default.SendPeriod = value;
				Properties.Settings.Default.Save();
			}
		}

		//======================================================================

		#endregion

		static Settings()
		{
			Properties.Settings.Default.SettingsKey = "NetScanner";
		}

		#region МЕТОДЫ

		//======================================================================
		// Выбор наиболее подходящего интерфейса
		//======================================================================

		private static
		IPAddress SelectInterface(
			)
		{
			// Подгружаем список сетевых адаптеров
			var adapters = NetworkInterface.GetAllNetworkInterfaces();
			
			NetworkInterface chosenAdapter = null;
			Boolean wifiAdapterFound = true;

			// Ищем подходящий сетевой адаптер
			foreach ( var adapter in adapters )
			{
				switch ( adapter.NetworkInterfaceType )
				{
					// Нашли Wi-Fi адаптер
					case NetworkInterfaceType.Wireless80211:
						wifiAdapterFound = true;
						chosenAdapter = adapter;
						break;
					
					// Пропускаем не интересующие нас адаптеры
					case NetworkInterfaceType.Loopback:
					case NetworkInterfaceType.Tunnel:
						break;

					default:
						if ( chosenAdapter == null )
							chosenAdapter = adapter;
						break;
				}

				if ( wifiAdapterFound )
					break;
			}

			if ( chosenAdapter == null )
				throw new Exception( "Не найдено подходящий сетевых адаптеров." );

			// Берём первый попавшийся IPv4 Unicast-адрес адаптера
			var ipProperties = chosenAdapter.GetIPProperties();
			
			foreach ( var unicastAddress in ipProperties.UnicastAddresses )
			{
				var family = unicastAddress.Address.AddressFamily;

				if ( family == AddressFamily.InterNetwork )
				{
					return unicastAddress.Address;
				}
			}

			throw new Exception( "У найденного адаптера нет IPv4 адресов." );
		}

		//======================================================================

		#endregion
	}
}
